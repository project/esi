<?php
/**
 * @file
 */

/**
 * Implements hook_esi_panels_context_arguments().
 *
 * Implementation provided on behalf of page_manager, which defines these task
 * plugins.
 */
function page_manager_esi_panels_context_arguments($task, $subtask = '', $args = array()) {
  switch ($task) {
    // The blog, poll, and contact_site tasks don't provide default context.
    case 'blog':
    case 'poll':
    case 'contact_site':
      return array();

    // The forum task provides the forum term object.
    case 'forum':
      $tid = empty($args) ? 0 : array_shift($args);
      $forum_term = module_exists('advanced_forum') ? advanced_forum_forum_load($tid) : forum_forum_load($tid);
      return array($forum_term->tid);

    // The blog_user, and contact_user tasks provide a user-object.
    case 'blog_user':
    case 'contact_user':
      $uid = array_shift($args);
      $account = user_load($uid);
      return array($account);

    // The comment_reply task provide a node object and a comment CID.
    case 'comment_reply':
      // Path is comment/reply/%node
      $nid = array_shift($args);
      $pid = array_shift($args);
      $node = node_load($nid);
      return array($node, $pid);

    // The node_edit and node_view tasks provide a node object.
    case 'node_edit':
    case 'node_view':
      $nid = array_shift($args);
      $node = node_load($nid);

      // Support node revisions by getting the
      // revision list for the current node.
      $vids = node_revision_list($node);
      // Assume next argument is the current $vid.
      $vid = array_shift($args);
      // Check if argument matches one of the $vids. If so, load the revision.
      if (!empty($vid) && array_key_exists($vid, $vids)) {
        $node = node_load($nid, $vid);
      }
      return array($node);

    case 'search':
      // @TODO.
      // return array($keys);

    case 'term_view':
      // @TODO.
      //  return array($terms, $depth);

    case 'user_view':
      $uid = array_shift($args);
      $account = user_load($uid);
      return array($account);
  }
}
